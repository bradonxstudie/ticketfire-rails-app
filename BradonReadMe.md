# Thought Process
Having never worked in RoR beforehand, this was a lot of fun. A lot of my path to solving the problems was initially conceived by thinking "How do I know how to do this in another environment?" and then finding a way to translate it to the project. 
Coming in to another entity's codebase, I first see how it functions before I touch it, prod it around, see what makes it tick. After this I assess the changes I need to make and how they fit into the pre-existing ecosystem, I make only the necessary changes in the most eloquent way I can to achieve the MVP. 

# Take-Aways
I thoroughly enjoyed my experience in RoR. My initial reading on it is that it achieves a stellar balance between giving you the power and greasing the gears for you. It is probably the most fun I have had coding in a while, and I only see my fulfillment going up as my skill increases with the technology. 

# Changelog
<ul>
    <li>Setup Rails</li>
    <li>Setup "rake"</li>
    <li>"When you HTTParty, you must party hard!" funny lol</li>
    <li>Installed sqlite</li>
    <li>Not working cool lol</li>
    <li>Uninstalled sqlite gem</li>
    <li>Gem 'sqlite3', git: Fixed with: "https://github.com/larskanis/sqlite3-ruby", branch:"add-gemspec"</li>
    <li>Used will_paginate for pagination</li>
    <li>Routed navigation with links</li>
    <li>Implemented links to manage list order</li>
    <li>Implemented order by jobs for specific department</li> 
</ul>

# Author Info
Email: bstudebaker17@gmail.com
<br>Phone: (937)844-0250